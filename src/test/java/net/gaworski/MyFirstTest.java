package net.gaworski;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static junit.framework.TestCase.assertTrue;

public class MyFirstTest {

    private static WebDriver browser;
    private static final String homePage = "http://selenium.gaworski.net";

    @BeforeClass
    public static void setup() {
        browser = new ChromeDriver();
    }

    @Test
    public void verifyGaworskiNetMentionsSeleniumTutorial() {
        pageShouldHaveText(homePage, "Selenium tutorial");

    }

    @Test
    public void verifyGaworskiNetMentionsTutorialSource() {
        pageShouldHaveText(homePage, "on Gaworski.net");
    }

    @AfterClass
    public static void teardown() {
        if (browser != null) browser.quit();
    }

    // ---

    private void pageShouldHaveText(String page, String text) {
        navigateToPage(homePage);
        verifyIfPageContainsText(text);
    }

    private void navigateToPage(String page) {
        browser.get(page);
    }

    private void verifyIfPageContainsText(String text) {
        assertTrue(browser.getPageSource().contains(text));
    }

}
